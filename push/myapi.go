package main

import (
	"encoding/json"
	"net/http"
	"fmt"
	"log"
	"github.com/ChimeraCoder/anaconda"
)

//	"github.com/davecgh/go-spew/spew"

type PowerTweetAPI struct {
	Url				string
	TwitterConfig	*anaconda.Configuration
}

func (pt *PowerTweetAPI) PtApiSetup(url string, twitterConfig *anaconda.Configuration) {
	pt.Url = url
	pt.TwitterConfig = twitterConfig
}

func (pt *PowerTweetAPI) PtNextTweet() map[string] interface{} {
	fullURL := fmt.Sprintf("%s/nextTweet", pt.Url)
	log.Println(fullURL)
	req, err := http.NewRequest("GET", fullURL, nil)
	if err != nil {
		log.Fatal("NewRequest failed : ", err)
	}
	client := new(http.Client)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do :", err)
	}
	defer resp.Body.Close()

	var r interface{}

	err = json.NewDecoder(resp.Body).Decode(&r)
	if err != nil {
		log.Fatal("json decode : ", err)
	}
	record := r.(map[string]interface{})
	title := (record["ptTweetTxt"]).(string)
	url := (record["url"]).(string)
	size := len(title) + 1 + pt.TwitterConfig.ShortUrlLengthHttps
	var tweet string
	if (size > 140) {
		max := 140 - 1 - pt.TwitterConfig.ShortUrlLengthHttps - 3
		tweet = fmt.Sprintf("%s... %s", title[0:max], url)
	} else {
		tweet = fmt.Sprintf("%s %s", title, url)
	}
	record["tweet"] = tweet;
	return record;
}

func (pt *PowerTweetAPI) PtSetStatus(id string, ptStatus string) {
	fullURL := fmt.Sprintf("%s/setStatus/%s/%s", pt.Url, id, ptStatus)
	log.Println(fullURL)
	req, err := http.NewRequest("POST", fullURL, nil)
	if err != nil {
		log.Fatal("NewRequest failed : ", err)
	}
	client := new(http.Client)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do :", err)
	}
	defer resp.Body.Close()
}
