var express = require('express');
var bodyParser = require('body-parser');
var mongo = require('mongodb');
var monk = require('monk');

var app = express();
var db = monk('mongodb://localhost/powertweet');

app.all('*', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Content-Type,X-Requested-With");
	next();
});

app.use(function (req, res, next) {
	req.db = db;
	next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', function (req, res) {
	res.send('Hello world');
})

app.get(
	'/list/:queue', 
	function (req, res) {
		var db = req.db;
		var collection = db.get('redditItems');
		var queue = req.params.queue;

		console.log("List : ", queue);

		if (queue === "pending")
			queue = null;
		collection.find({ptStatus : queue}, {}, 
			function(e, d) {
				res.send(d);
			}
		);
	}
);

app.get(
	'/nextTweet',
	function (req, res) {
		var db = req.db;
		var collection = db.get('redditItems');

		collection.findOne({ ptStatus : "validated" }, {},
			function(e, d) {
				res.send(d);
			}
		)
	}
);

app.post(
	'/setStatus/:id/:ptStatus',
	function (req, res) {
		var db = req.db;
		var collection = db.get('redditItems');

		const id = req.params.id;
		const ptStatus = req.params.ptStatus === "pending" ? null : req.params.ptStatus;

		console.log("id to update:", id);
		console.log("ptStatus :", ptStatus);

		collection.update(
			{ "_id" : id },
			{
				"$set" : {
					"ptStatus" : ptStatus,
				}
			}
		).then(
			function () {
				res.send();
			}
		)
	}
);

app.post(
	'/setTweetTxt/:id',
	function (req, res) {
		var db = req.db;
		var collection = db.get('redditItems');

		const id = req.params.id;
		const ptTweetTxt = req.body.ptTweetTxt;

		console.log("id to update:", id);
		console.log("ptTweetTxt :", ptTweetTxt);

		collection.update(
			{ "_id" : id },
			{
				"$set" : {
					"ptTweetTxt" : ptTweetTxt
				}
			}
		).then(
			function () {
				res.send();
			}
		)
	}
);

app.post(
	'/storeDocument',
	function (req, res) {
		var db = req.db;
		var collection = db.get('redditItems');

		const doc = req.body;

		collection.insert(
			 doc,
			 {}
		).then(
			function () {
				console.log("storeDocument: id =", doc.id, "inserted");
			},
			function () {
				console.log("storeDocument: id =", doc.id, "insertion failed, possibly already in the DB");
			}
		).then(
			function () {
				res.send()
			})
	}
);

app.listen(8081);
