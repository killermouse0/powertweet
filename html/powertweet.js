var app = angular.module('powerTweet', []);

app.
controller('validationCtrl', 
		function($scope, $http) {
			///////////
			// getItems
			///////////
			this.getItems = function (queue) {
				$http.get("/ptapi/list/"+queue)
					.then(
							function (response) {
								$scope.items = response.data;
								$scope.items.map( function(item) {
									if (typeof(item.ptTweetTxt) === 'undefined' ||
											item.ptTweetTxt === null ||
											item.ptTweetTxt === "") {
										item.ptTweetTxt = item.title;
									}
								});
								console.log("Got this: ", $scope.items);
								return;
							});
			};

			this.setStatus = function(id) {
				const _id = $scope.items[id]._id;
				const ptStatus = $scope.items[id].ptStatus;

				console.log("setStatus id = ", _id, " ptStatus = ", ptStatus);
				$http.post("/ptapi/setStatus/"+ _id + "/" + ptStatus);
			};

			this.setTweetTxt = function(id) {
				const _id = $scope.items[id]._id;
				const ptTweetTxt = $scope.items[id].ptTweetTxt;

				console.log("setTweetTxt id = ", _id, " ptTweetTxt = ", ptTweetTxt);
				$http.post("/ptapi/setTweetTxt/"+ _id,
						{ "ptTweetTxt" : ptTweetTxt});
			};

			this.getItems("pending");
		}
);
